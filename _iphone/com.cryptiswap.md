---
wsId: quantex
title: Quantex
altTitle: 
authors:
- danny
appId: com.cryptiswap
appCountry: us
idd: '1558998980'
released: 2021-04-19
updated: 2023-03-09
version: 1.0.8
stars: 4.8
reviews: 19
size: '31076352'
website: https://myquantex.com/
repository: 
issue: 
icon: com.cryptiswap.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-05-22
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: 

---

{% include copyFromAndroid.html %}
