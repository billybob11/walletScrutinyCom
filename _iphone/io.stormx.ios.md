---
wsId: stormX
title: StormX - Shop & Earn Crypto
altTitle: 
authors:
- danny
appId: io.stormx.ios
appCountry: us
idd: 1420545397
released: 2018-12-12
updated: 2023-05-10
version: 4.18.2
stars: 4.6
reviews: 1260
size: '66835456'
website: https://stormx.io/
repository: 
issue: 
icon: io.stormx.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: stormxio
social:
- https://www.linkedin.com/company/StormX
- https://www.facebook.com/stormxio
- https://www.reddit.com/r/stormxio
features: 
developerName: StormX Singapore PTE. LTD.

---

{% include copyFromAndroid.html %}
