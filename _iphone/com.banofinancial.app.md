---
wsId: banosuperapp
title: Bano - Connect Your Life
altTitle: 
authors:
- danny
appId: com.banofinancial.app
appCountry: au
idd: '1562849570'
released: 2021-05-13
updated: 2023-06-09
version: 1.4.90
stars: 4
reviews: 38
size: '90797056'
website: https://bano.app
repository: 
issue: 
icon: com.banofinancial.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-03-09
signer: 
reviewArchive: 
twitter: banosuperapp
social:
- https://www.facebook.com/banosuperapp
- https://www.instagram.com/banosuperapp
- https://www.linkedin.com/company/banoapp/
features: 
developerName: Bano Pty Ltd

---

{% include copyFromAndroid.html %}

