---
wsId: Cofinex
title: Cofinex Exchange
altTitle: 
authors:
- danny
appId: com.cofinex.app
appCountry: sg
idd: 1540245134
released: 2021-08-12
updated: 2021-08-12
version: '1.0'
stars: 
reviews: 
size: 52673536
website: https://exchange.cofinex.io/
repository: 
issue: 
icon: com.cofinex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-10
signer: 
reviewArchive: 
twitter: CofinexExchange
social:
- https://www.linkedin.com/company/cofinex-exchange
- https://www.facebook.com/cofinex.io
- https://www.youtube.com/@cofinexexchange
features: 
developerName: 

---

{% include copyFromAndroid.html %}