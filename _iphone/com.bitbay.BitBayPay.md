---
wsId: bitcointerminal.zondapay
title: zondacrypto pay terminal
altTitle: 
authors:
- danny
appId: com.bitbay.BitBayPay
appCountry: us
idd: '1205935259'
released: 2017-03-17
updated: 2023-05-08
version: 2.1.2
stars: 0
reviews: 0
size: '20752384'
website: https://zondaglobal.com
repository: 
issue: 
icon: com.bitbay.BitBayPay.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-05-10
signer: 
reviewArchive: 
twitter: zondaglobal
social:
- https://www.linkedin.com/company/zondaglobal/
- https://www.facebook.com/ZondaGlobal/
- https://www.instagram.com/zondaglobal/
features: 
developerName: BitBay Sp. z o.o.

---

{% include copyFromAndroid.html %}
