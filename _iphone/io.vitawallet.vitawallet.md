---
wsId: vita
title: Vita Wallet
altTitle: 
authors:
- danny
appId: io.vitawallet.vitawallet
appCountry: cl
idd: 1486999955
released: 2019-11-15
updated: 2023-06-15
version: 5.4.4
stars: 4.7
reviews: 117
size: '56904704'
website: https://www.vitawallet.io
repository: 
issue: 
icon: io.vitawallet.vitawallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: vitawallet
social:
- https://www.linkedin.com/company/vita-wallet
- https://www.facebook.com/vitawallet
features: 
developerName: Vita Solutions S.p.A

---

{% include copyFromAndroid.html %}
