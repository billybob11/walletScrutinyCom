---
wsId: satangPro
title: 'Satang Pro: Bitcoin Exchange'
altTitle: 
authors:
- danny
appId: com.satang-pro.ios
appCountry: us
idd: '1513155132'
released: 2021-08-25
updated: 2023-05-15
version: 1.4.8
stars: 3.7
reviews: 6
size: '173169664'
website: https://satangcorp.com
repository: 
issue: 
icon: com.satang-pro.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-06-28
signer: 
reviewArchive: 
twitter: SatangOfficial_
social:
- https://www.facebook.com/satangcorp
- https://www.youtube.com/channel/UC4U0T8JSB82E5JW6ATKKqiw
- https://www.instagram.com/satang.official/
features: 
developerName: Satang Corporation Co., Ltd.

---

{% include copyFromAndroid.html %}
