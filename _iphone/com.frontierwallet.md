---
wsId: frontierDeFi
title: 'Frontier: Crypto & DeFi Wallet'
altTitle: 
authors:
- danny
appId: com.frontierwallet
appCountry: in
idd: 1482380988
released: 2019-11-05
updated: 2023-06-16
version: 5.8.0
stars: 4.9
reviews: 41
size: '123122688'
website: https://frontier.xyz
repository: 
issue: 
icon: com.frontierwallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-13
signer: 
reviewArchive: 
twitter: FrontierDotXYZ
social: 
features: 
developerName: Ohana Labs Pte. Ltd.

---

{% include copyFromAndroid.html %}
