---
wsId: liskMobile
title: Lisk
altTitle: 
authors:
- danny
appId: io.lisk.mobile
appCountry: us
idd: '1436809559'
released: 2018-10-05
updated: 2022-03-21
version: 2.1.1
stars: 4.7
reviews: 47
size: '25532416'
website: https://lisk.com/wallet
repository: 
issue: 
icon: io.lisk.mobile.jpg
bugbounty: 
meta: stale
verdict: nobtc
date: 2023-05-05
signer: 
reviewArchive: 
twitter: LiskHQ
social:
- https://www.linkedin.com/company/lisk
- https://www.facebook.com/LiskHQ
- https://www.youtube.com/channel/UCuqpGfg_bOQ8Ja4pj811PWg
- https://t.me/Lisk_HQ
- https://www.instagram.com/lisk_blockchain
features: 
developerName: Lisk Stiftung

---

{% include copyFromAndroid.html %}