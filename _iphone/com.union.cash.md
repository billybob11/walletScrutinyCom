---
wsId: unionCash
title: Unioncash
altTitle: 
authors:
- danny
appId: com.union.cash
appCountry: us
idd: '1576165600'
released: 2021-07-14
updated: 2023-03-09
version: '1.32'
stars: 4.7
reviews: 25
size: '136509440'
website: https://union.cash
repository: 
issue: 
icon: com.union.cash.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-07
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Unioncash EU UAB

---

{% include copyFromAndroid.html %}