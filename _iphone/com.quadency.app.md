---
wsId: quadency
title: Quadency - Crypto Platform
altTitle: 
authors:
- danny
appId: com.quadency.app
appCountry: us
idd: '1573602424'
released: 2021-10-31
updated: 2023-03-12
version: 1.6.0
stars: 4.2
reviews: 22
size: '44139520'
website: https://quadency.com
repository: 
issue: 
icon: com.quadency.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-29
signer: 
reviewArchive: 
twitter: quadency
social:
- https://www.linkedin.com/company/quadency/
- https://www.facebook.com/quadency
features: 
developerName: Quadency Inc

---

{% include copyFromAndroid.html %}