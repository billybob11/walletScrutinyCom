---
wsId: quantfury
title: 'Quantfury: Trading Made Honest'
altTitle: 
authors:
- danny
appId: com.quantfury
appCountry: gb
idd: 1445564443
released: 2018-12-15
updated: 2023-06-09
version: 1.60.2
stars: 4.5
reviews: 52
size: '70170624'
website: https://quantfury.com/
repository: 
issue: 
icon: com.quantfury.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-01
signer: 
reviewArchive: 
twitter: quantfury
social: 
features: 
developerName: Quantfury Ltd

---

{% include copyFromAndroid.html %}
