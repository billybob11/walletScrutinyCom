---
wsId: tomyaApp
title: Tomya
altTitle: 
authors:
- danny
appId: com.tomyaapp.ios
appCountry: tr
idd: '1537333567'
released: 2020-11-07
updated: 2022-07-29
version: 1.2.5
stars: 3.8
reviews: 21
size: '70612992'
website: https://www.tomya.com
repository: 
issue: 
icon: com.tomyaapp.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: TOMYA TEKNOLOJİ A.Ş.

---

{% include copyFromAndroid.html %}