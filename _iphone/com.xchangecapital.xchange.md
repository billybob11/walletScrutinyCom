---
wsId: xchangeCash
title: Xchange cash - купить Биткоин
altTitle: 
authors:
- danny
appId: com.xchangecapital.xchange
appCountry: ru
idd: '1520392383'
released: 2020-07-25
updated: 2022-12-22
version: 1.0.2
stars: 5
reviews: 5
size: '80513024'
website: 
repository: 
issue: 
icon: com.xchangecapital.xchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-10
signer: 
reviewArchive: 
twitter: XchangeO
social:
- https://xchange.ltd
- https://vk.com/xchange_cc
- https://t.me/Xchange_official
features: 
developerName: XChangeCapitalGroup

---

{% include copyFromAndroid.html %}
