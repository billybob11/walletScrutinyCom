---
wsId: tatspace
title: TATSPACE
altTitle: 
authors:
- danny
appId: com.tatspaceapp
appCountry: us
idd: '1629762278'
released: 2022-06-21
updated: 2023-06-16
version: 1.16.0
stars: 4
reviews: 41
size: '87532544'
website: https://www.tatcoin.com
repository: 
issue: 
icon: com.tatspaceapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-12
signer: 
reviewArchive: 
twitter: tatspaceapp
social:
- https://www.linkedin.com/company/the-abit-app/
- https://t.me/tatspaceapp
- https://www.instagram.com/tatspaceapp/
features: 
developerName: TATSPACE

---

{% include copyFromAndroid.html %}
