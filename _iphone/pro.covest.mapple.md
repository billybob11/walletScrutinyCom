---
wsId: covest
title: COVEST – 코베스트
altTitle: 
authors:
- danny
appId: pro.covest.mapple
appCountry: vn
idd: '1627431920'
released: 2022-07-15
updated: 2022-07-15
version: '1.8'
stars: 0
reviews: 0
size: '10631168'
website: 
repository: 
issue: 
icon: pro.covest.mapple.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-17
signer: 
reviewArchive: 
twitter: COVEST_Official
social:
- https://t.me/covestpro
features: 
developerName: 

---

{% include copyFromAndroid.html %}