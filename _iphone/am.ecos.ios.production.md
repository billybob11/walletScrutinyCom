---
wsId: ECOS
title: 'ECOS: Bitcoin Mining & Wallet'
altTitle: 
authors:
- danny
appId: am.ecos.ios.production
appCountry: us
idd: 1528964374
released: 2020-11-25
updated: 2023-06-16
version: 1.34.1
stars: 3.7
reviews: 155
size: '88565760'
website: https://ecos.am/
repository: 
issue: 
icon: am.ecos.ios.production.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive: 
twitter: ecosmining
social:
- https://www.facebook.com/ecosdefi
features: 
developerName: Ecos am

---

{% include copyFromAndroid.html %}
