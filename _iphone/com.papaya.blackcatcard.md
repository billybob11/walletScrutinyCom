---
wsId: blackcatcard
title: 'Blackсatсard: my prepaid card'
altTitle: 
authors:
- danny
appId: com.papaya.blackcatcard
appCountry: lv
idd: 1449352913
released: 2019-03-07
updated: 2023-06-01
version: 1.2.32
stars: 4
reviews: 60
size: '258076672'
website: https://blackcatcard.com
repository: 
issue: 
icon: com.papaya.blackcatcard.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Papaya Ltd

---

{% include copyFromAndroid.html %}