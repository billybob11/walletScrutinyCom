---
wsId: theBitcoinCompany
title: The Bitcoin Company Wallet
altTitle: 
authors:
- danny
appId: com.thebitcoincompany.app.ios
appCountry: us
idd: '1600995023'
released: 2022-03-17
updated: 2023-05-08
version: '1.28'
stars: 4.8
reviews: 19
size: '81538048'
website: https://thebitcoincompany.com
repository: 
issue: 
icon: com.thebitcoincompany.app.ios.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-06-03
signer: 
reviewArchive: 
twitter: thebtcco
social:
- https://www.facebook.com/thebtcco
- https://www.linkedin.com/company/thebitcoincompany
- https://www.instagram.com/thebtcco
- https://t.me/thebitcoincompany
features: 
developerName: Two Twenty Two, Inc.

---

{% include copyFromAndroid.html %}