---
wsId: coinzix
title: Coinzix
altTitle: 
authors:
- danny
appId: com.servotech.coinzix
appCountry: de
idd: '1586167950'
released: 2022-02-09
updated: 2023-03-28
version: '1.9'
stars: 4.9
reviews: 12
size: '9564160'
website: 
repository: 
issue: 
icon: com.servotech.coinzix.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-21
signer: 
reviewArchive: 
twitter: coinzixcom
social:
- https://www.facebook.com/coinzixcom
- https://www.instagram.com/coinzixcom
- https://t.me/coinzixCEX
features: 
developerName: COINZIX S.R.L.

---

{% include copyFromAndroid.html %}

