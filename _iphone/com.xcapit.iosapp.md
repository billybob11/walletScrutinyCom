---
wsId: xcapit
title: Xcapit
altTitle: 
authors:
- danny
appId: com.xcapit.iosapp
appCountry: ar
idd: '1545648148'
released: 2020-12-28
updated: 2023-06-15
version: 3.21.1
stars: 4.5
reviews: 28
size: '61020160'
website: https://xcapit.com
repository: 
issue: 
icon: com.xcapit.iosapp.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-07-13
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Cryptolab SAS

---

{% include copyFromAndroid.html %}