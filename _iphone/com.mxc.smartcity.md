---
wsId: datadash
title: DataDash App
altTitle: 
authors:
- danny
appId: com.mxc.smartcity
appCountry: us
idd: 1509218470
released: 2020-06-30
updated: 2023-06-07
version: 6.3.0
stars: 3.6
reviews: 115
size: '108748800'
website: http://mxc.org
repository: 
issue: 
icon: com.mxc.smartcity.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-01
signer: 
reviewArchive: 
twitter: mxcfoundation
social:
- https://www.facebook.com/MXCfoundation
- https://www.reddit.com/r/MXC_Foundation
features: 
developerName: MXC Foundation LLC

---

{% include copyFromAndroid.html %}
