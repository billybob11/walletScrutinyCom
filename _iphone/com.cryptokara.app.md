---
wsId: cryptoKara
title: CryptoKara
altTitle: 
authors:
- danny
appId: com.cryptokara.app
appCountry: us
idd: '1581610129'
released: 2021-10-19
updated: 2022-06-25
version: 1.4.0
stars: 4
reviews: 34
size: '41851904'
website: https://crypto-kara-site.vercel.app/
repository: 
issue: 
icon: com.cryptokara.app.jpg
bugbounty: 
meta: stale
verdict: nosource
date: 2023-06-20
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: AUTOMATED CHAIN LIMITED

---

{% include copyFromAndroid.html %}
