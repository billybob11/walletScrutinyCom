---
wsId: geminiwallet
title: 'Gemini: Buy Bitcoin & Crypto'
altTitle: 
authors:
- leo
appId: com.gemini.ios
appCountry: 
idd: 1408914447
released: 2018-12-11
updated: 2023-06-18
version: 23.613.1
stars: 4.8
reviews: 94475
size: '144603136'
website: http://gemini.com
repository: 
issue: 
icon: com.gemini.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-01-04
signer: 
reviewArchive: 
twitter: gemini
social:
- https://www.linkedin.com/company/geminitrust
- https://www.facebook.com/GeminiTrust
features: 
developerName: Gemini Trust Company, LLC

---

This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.
