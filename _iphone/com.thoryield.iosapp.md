---
wsId: thorYieldApp
title: THORYield
altTitle: 
authors:
- danny
appId: com.thoryield.iosapp
appCountry: us
idd: '1618239586'
released: 2022-04-13
updated: 2023-05-21
version: 2.0.34
stars: 4.4
reviews: 10
size: '76042240'
website: https://thoryield.com/
repository: 
issue: 
icon: com.thoryield.iosapp.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Inflekta S.A.

---

{% include copyFromAndroid.html %}
