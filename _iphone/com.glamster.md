---
wsId: glamster
title: Glamster
altTitle: 
authors:
- danny
appId: com.glamster
appCountry: de
idd: '1528839992'
released: 2020-09-14
updated: 2023-06-19
version: 4.1.0
stars: 4.6
reviews: 241
size: '119735296'
website: https://glamster.io/
repository: 
issue: 
icon: com.glamster.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-03-31
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Glamster

---

{% include copyFromAndroid.html %}

