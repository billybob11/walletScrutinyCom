---
wsId: LMAXDigital
title: LMAX Digital Trading
altTitle: 
authors:
- danny
appId: com.mobiletradingpartners.lmaxdigital
appCountry: us
idd: '1375900638'
released: 2018-06-11
updated: 2023-06-12
version: 4.4.302
stars: 0
reviews: 0
size: '134766592'
website: https://www.lmax.com/mobile
repository: 
issue: 
icon: com.mobiletradingpartners.lmaxdigital.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-04
signer: 
reviewArchive: 
twitter: LMAX
social:
- https://www.linkedin.com/company/lmax-group
features: 
developerName: LMAX DIGITAL EXCHANGE LIMITED

---

{% include copyFromAndroid.html %}
