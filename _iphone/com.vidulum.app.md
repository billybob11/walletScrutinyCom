---
wsId: Vidulum
title: Vidulum
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2023-04-25
version: 1.5.2
stars: 4.3
reviews: 11
size: '70052864'
website: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: VidulumApp
social:
- https://www.facebook.com/VidulumTeam
- https://www.reddit.com/r/VidulumOfficial
features: 
developerName: Vidulum LLC

---

{% include copyFromAndroid.html %}
