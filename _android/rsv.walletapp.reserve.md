---
wsId: 
title: Reserve
altTitle: 
authors: 
users: 500000
appId: rsv.walletapp.reserve
appCountry: 
released: 2019-07-30
updated: 2023-02-20
version: 2.2.0
stars: 4.1
ratings: 
reviews: 419
size: 
website: http://www.reserve.org
repository: 
issue: 
icon: rsv.walletapp.reserve.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-02
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Reserve, Inc.
features: 

---

