---
wsId: unionCash
title: Unioncash
altTitle: 
authors:
- danny
users: 1000
appId: com.union.cash
appCountry: 
released: 2021-06-25
updated: 2023-04-27
version: 1.2.4
stars: 
ratings: 
reviews: 
size: 
website: http://www.union.cash/
repository: 
issue: 
icon: com.union.cash.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-06-07
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Union Cash
features: 

---

## App Description from Google Play 

> UnionCash function introduction
-  Buy cryptocurrency
Users can use UnionCash to purchase cryptocurrencies (Bitcoin, Ethereum, USDT, etc.), and payment methods can be bank cards or bank accounts.
- Receive cryptocurrency
UnionCash will open a cryptocurrency wallet for each customer, and users can use the cryptocurrency wallet to receive cryptocurrency transfers from other platforms.
- Transfer out cryptocurrency
Users can use UnionCash's cryptocurrency wallet to send cryptocurrencies to other platforms.
- Sell cryptocurrency
Users can use UnionCash to convert cryptocurrency into fiat currency and deposit them into a Union Cash Euro account.
- UnionCash Euro account withdrawal
UnionCash Euro accounts support withdrawals to international bank accounts (IBAN).

## Analysis 

- We had difficulty with registration as the SMS and email verification took a while. 
- But based on the description, the app can:
   - receive cryptocurrency 
   - transfer out cryptocurrency 
- After using a temporary phone number, we were able to register. 
- [Screenshots](https://twitter.com/BitcoinWalletz/status/1666386462923968515)
- The app's BTC receive function seems to be "temporarily" unavailable, as the app's error message describes it. 
- Most of the features of the app are not available prior to KYC. 
- We emailed the developer contact: service@union.cash
- This could either go two ways: custodial or no-btc support. 
- We'll mark this as a **work-in-progress** until more information is available.