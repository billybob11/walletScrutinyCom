---
wsId: 
title: 'Elyps: Think out of the Bank'
altTitle: 
authors:
- danny
users: 1000
appId: com.elyps
appCountry: 
released: 2020-04-29
updated: 2023-03-29
version: 10.4.0
stars: 
ratings: 
reviews: 
size: 
website: https://elyps.com
repository: 
issue: 
icon: com.elyps.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-05-24
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: Elyps
features: 

---

## App Description from Google Play 

> Regulated in Europe and the UK, Elyps is the pioneer payment platform leveraging blockchain technology since 2018.
>
> - Fintech: Build financial services on top of our SaaS platform
- Trade Finance: Secure challenging financial transactions
- Fund Managers: Streamline AIF Depository account and LP onboarding

### [Terms](https://support.elyps.com/en/support/solutions/articles/80000824343-payrnet-limited-terms-and-conditions) 

> When we receive money from you or on your behalf, this money will be held by us in the relevant Safeguarded Account in exchange for the issuance by us to you of Electronic Money. Your funds will not be used by us for any other purpose and in the unlikely event that we become insolvent, your e-money is protected in an EEA-authorised credit institution or the Bank of England.

## Analysis 

- We cannot install the app as its registration is by invite only and geolocation-restricted
- We did not find any reference to 'Bitcoin' or 'crypto'  
- We will email them to ask for more information at: hello@elyps.com 
- We'll keep this as a work-in-progress until more information is available.


