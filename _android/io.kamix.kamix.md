---
wsId: 
title: Kamix
altTitle: 
authors:
- danny
users: 5000
appId: io.kamix.kamix
appCountry: 
released: 2019-03-14
updated: 2023-04-18
version: 2.1.6
stars: 
ratings: 
reviews: 
size: 
website: https://kamix.io
repository: 
issue: 
icon: io.kamix.kamix.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-05-05
signer: 
reviewArchive: 
twitter: KamixApp
social:
- https://www.linkedin.com/company/kamixapp
- https://www.facebook.com/Kamixapp
- https://www.instagram.com/kamixapp
redirect_from: 
developerName: Kamix R&D
features: 

---

## App Description from Google Play 

> Kamix allows you to buy and store crypto assets in your wallet, that you can send to your recipient's wallet in Cameroon with 0% commission fees. Before using this service, it is mandatory to validate your identity as required by Anti Money Laundering and Terrorism Financing regulations.
>
> Once your recipient receives the crypto assets, the app allows him to automatically sell it and receive franc CFA directly in his mobile money account Orange Money or MTN Momo. Therefore you must indicate your recipient's mobile money number in each transaction.

## Analysis 

- We installed the app but were not able to register due to AML/KYC and regional requirements. We did however, manage to register on the web app which only required an email address. 
- The Kamix.io website also has a blockchain explorer 
- We contacted Kamix via [twitter](https://twitter.com/BitcoinWalletz/status/1654326997353627648) to clarify.
- From the web app, the user can manually input his own BTC address. 
- From the description, it would seem that the sender will send BTC, however, the recipient will receive Franc CFA in his mobile money account.
- We will also send an email with the same queries. 
- For now, we will keep this app as a **work-in-progress**. 
