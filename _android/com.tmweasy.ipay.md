---
wsId: 
title: Ipay Exchange
altTitle: 
authors:
- danny
users: 1000
appId: com.tmweasy.ipay
appCountry: 
released: 2020-08-12
updated: 2022-01-23
version: '1.3'
stars: 
ratings: 
reviews: 
size: 
website: https://www.ipay-ex.com/
repository: 
issue: 
icon: com.tmweasy.ipay.png
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-06-03
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: tmweasyapp
features: 

---

## App Description from Google Play

> Ipay Exchange Securely exchange Paypal and Bitcoin. Withdraw Paypal to bank instantly 24 hours a day. 

## Analysis 

- The app is an exchange service specifically covering the Thailand geographic. 
- Bitcoin to bank account, Paypal and other eWallet exchange services are included. 
- The app itself **has no Bitcoin wallet** and merely reflects the website.