---
wsId: 
title: Cryppter
altTitle: 
authors:
- danny
users: 1000
appId: com.cryppter.app.release
appCountry: 
released: 2020-05-21
updated: 2022-08-22
version: 0.0.144 (7723)
stars: 5
ratings: 
reviews: 12
size: 
website: https://cryppter.com/
repository: 
issue: 
icon: com.cryppter.app.release.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-05-17
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: 
features: 

---

This product has previously been marked as defunct, but as of May 17, 2023, seems to be available again. 

## App Description from Google Play 

> This is a free wallet app which offers multiple features.
> - Supported coins: BTC, LTC, ETH.
> - All payments are being originated directly from the wallet app, which locally signs all transactions.
>
> Our server does not store any sensitive data and we cannot restore your coins if you lost the seed phrase and Private Keys.

## Analysis 

- We mailed suppport@cryppter.com to ask whether the app is source-available. 
- It has a BTC wallet that can send/receive.
- On the Google Play description, we did not find any indications that it is **source-available**. 